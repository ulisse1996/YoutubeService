import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SearchResult } from './search-result.model';
import { Token } from './token.model'
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class YoutubeSearchService {

  resultsList : SearchResult[] = [];
  apiKey : string = "AIzaSyDDvkz2YjX09caqtCaYDo7v2yCiX6INCIk";
  apiUrl : string = "https://www.googleapis.com/youtube/v3/search?"
  apiUrlVideo : string = "https://www.googleapis.com/youtube/v3/videos?"
  tokens : Token;

  constructor(private http : HttpClient) { }

  resultCostructor(data) {
    const items : Object[] = data['items'];
    let i : number;
    for (i = 0; i < items.length ; ++i) {
      let result : SearchResult = new SearchResult(
        items[i]['id']['videoId'],
        items[i]['snippet']['title'],
        items[i]['snippet']['description'],
        items[i]['snippet']['thumbnails']['high']['url']
      )
      this.resultsList.push(result);
    }
    this.tokens = new Token(data['nextPageToken'],data['prevPageToken']);
    let results : any[] = [];
    results.push(this.resultsList);
    results.push(this.tokens);
    return results;
  }

  setVideo(data) : SearchResult {
    const items : Object[] = data['items'];
    let result = new SearchResult(
      items[0]['id'],
      items[0]['snippet']['title'],
      items[0]['snippet']['description'],
      items[0]['snippet']['thumbnails']['high']['url']
    );
    return result;
  }

  search(query : string,choose : number,token : string) : Observable<any[]> {
    this.resultsList.length = 0;
    let params : string;
    if (choose === 0) {
     params = [
      'part=snippet',
      'maxResults=12',
      'type=video',
      `q=${query}`,
      `key=${this.apiKey}`,
      `pageToken=${token}`,
      `location=${localStorage.getItem("pos")}`,
      'locationRadius=50km'
    ].join('&');
    } else if(choose === 1) {
      params = [
        'part=snippet',
        'maxResults=12',
        'type=video',
        `q=${query}`,
        `key=${this.apiKey}`,
        `location=${localStorage.getItem("pos")}`,
        'locationRadius=50km'
      ].join('&');
    } else {
      params = [
        'part=snippet',
        'maxResults=12',
        'type=video',
        `q=${query}`,
        `key=${this.apiKey}`,
        `pageToken=${token}`,
        `location=${localStorage.getItem("pos")}`,
        'locationRadius=50km'
      ].join('&');
    }
    const queryUrl = `${this.apiUrl}${params}`;
    console.log(queryUrl)
    return this.http.get(queryUrl).pipe(map(data => this.resultCostructor(data)));
  }

  singleVideo(id : string) : Observable<SearchResult> {
    let params : string = [
      'part=snippet',
      `id=${id}`,
      `key=${this.apiKey}`
    ].join("&");
    const queryUrl = `${this.apiUrlVideo}${params}`;
    return this.http.get(queryUrl).pipe(map(data => this.setVideo(data)));
  }

}
