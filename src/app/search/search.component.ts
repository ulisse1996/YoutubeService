import { Component } from '@angular/core';
import { YoutubeSearchService } from './youtube-search.service';
import { SearchResult } from './search-result.model';
import { Token } from './token.model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent {

  results : SearchResult[];
  tokens : Token

  constructor(private youtubeSearch : YoutubeSearchService) {}

  search(query : string,scelta : number) {
    if (scelta === 0) {
      this.youtubeSearch.search(query,scelta,this.tokens.precToken).subscribe(result => {
        this.results = result[0];
        this.tokens = result[1];
      })
    } else if (scelta === 1) {
      this.youtubeSearch.search(query,scelta,null).subscribe(result => {
        this.results = result[0];
        this.tokens = result[1];
      })
    } else if (scelta == 2) {
      this.youtubeSearch.search(query,scelta,this.tokens.succToken).subscribe(result => {
        this.results = result[0];
        this.tokens = result[1];
      })
    }
  }

  isSucc() : boolean {
    if (this.tokens !== undefined) {
      if (this.tokens.succToken !== undefined) {
        return true;
      }
    }
    return false;
  }

  isPrec() : boolean {
    if (this.tokens !== undefined) {
      if (this.tokens.precToken !== undefined) {
        return true;
      }
    }
    return false;
  }

}
