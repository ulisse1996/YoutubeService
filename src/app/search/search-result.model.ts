export class SearchResult {
    id : string;
    title : string;
    description : string;
    thumbnailUrl : string;
    videoUrl : string;

    constructor (id : string,title : string,description : string,thumbnailUrl : string) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.thumbnailUrl = thumbnailUrl;
        this.videoUrl = `https://www.youtube.com/watch?v=${this.id}`;
    }
}