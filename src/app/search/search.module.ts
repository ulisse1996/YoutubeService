import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './search.component';
import { VideoComponent } from './video/video.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { Routes, RouterModule } from '@angular/router';
import { YoutubeSearchService } from './youtube-search.service';
import { StatusInterceptor } from './status.interceptor';

const routes : Routes = [
  {
    path : '',
    component : SearchComponent,
    pathMatch : 'full'
  },
  {
    path : ':id',
    component : VideoComponent,
    pathMatch : 'full'
  }
]

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SearchComponent, VideoComponent],
  providers : [YoutubeSearchService,{
    provide : HTTP_INTERCEPTORS,
    useClass : StatusInterceptor,
    multi : true
  }]
})
export class SearchModule { }
