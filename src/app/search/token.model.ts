export class Token {

    succToken : string;
    precToken : string;

    constructor(succ : string , prec : string) {
        this.succToken = succ;
        this.precToken = prec;
    }

}