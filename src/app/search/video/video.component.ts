import { Component, OnInit , Input} from '@angular/core';
import { SearchResult } from '../search-result.model';
import { YoutubeSearchService } from '../youtube-search.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit {

  @Input()
  result : SearchResult;

  constructor(private youteSearch : YoutubeSearchService,private sanitizer : DomSanitizer) { }

  ngOnInit() {
    if (this.result === undefined) {
      this.youteSearch.singleVideo(location.href.substr("http://localhost:4200/search/".length)).subscribe( result => {
        this.result = result;
      })
    }
  }

  isRedirected() : boolean {
    if (location.href.substring('http://localhost:4200/search'.length).length > 0) {
      return true;
    }
    return false;
  }

  getUrl() {
    let url = this.result.videoUrl.replace('watch?v=','embed/');
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

}
