export class Info {
    name : string;
    description : string;
    subDate : string;
    thumbnailUrl : string
    country : string;
    videoNum : number;
    viewsCount : number;
    subsCount : number;

    constructor(name : string, description : string , subDate : string , thumbnailUrl : string, country : string , videoNum : number , viewsCount : number , subsCount : number ) {
        this.name = name;
        this.description = description;
        this.subDate = subDate;
        this.thumbnailUrl = thumbnailUrl;
        this.country = country;
        this.videoNum = videoNum;
        this.viewsCount = viewsCount;
        this.subsCount = subsCount;
    }
}