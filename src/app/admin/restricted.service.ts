import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'
import { Info } from './info.model';

@Injectable({
  providedIn: 'root'
})
export class RestrictedService {

  apiKey : string = "AIzaSyDDvkz2YjX09caqtCaYDo7v2yCiX6INCIk";
  apiUrl : string = "https://www.googleapis.com/youtube/v3/channels?"
  analyticsUrl : string = "https://youtubeanalytics.googleapis.com/v2/reports?"

  constructor(private http : HttpClient) { }

  auth() : void {
    window.open('https://accounts.google.com/o/oauth2/v2/auth?scope=https://www.googleapis.com/auth/youtube.readonly&include_granted_scopes=true&state=state_parameter_passthrough_value&redirect_uri=http://localhost:4200/admin&response_type=token&client_id=713162778332-ak66gcat70nota2sdsvimnreo5mo5jbd.apps.googleusercontent.com', 
    'newwindow', 
    'width=400,height=600');
  }

  setAuth() {
    window.opener.sessionStorage.setItem('auth',location.href);
    window.self.close();
    window.opener.location.reload();
  }

  getToken(token : string) : string {
    let params : string[] = token.split("&");
    return params[1];
  }

  setChannelInfo(data) : Info {
    let info : Info;
    const items : Object[] = data['items'];
      info = new Info(
        items[0]['snippet']['title'],
        items[0]['snippet']['description'],
        items[0]['snippet']['publishedAt'],
        items[0]['snippet']['thumbnails']['high']['url'],
        items[0]['snippet']['country'],
        items[0]['statistics']['videoCount'],
        items[0]['statistics']['viewCount'],
        items[0]['statistics']['subscriberCount']
      )
    return info;
  }

  setAnalytics(data) : string[][] {
    let analytics : string[][] = data['rows'];
    return analytics;
  }

  channel(token : string) : Observable<Info> {
    token = this.getToken(token);
    let params = [
      `${token}`,
      'part=snippet,contentDetails,statistics',
      'mine=true',
      `key=${this.apiKey}`
    ].join('&');
    let queryUrl = `${this.apiUrl}${params}`;
    return this.http.get(queryUrl).pipe((map(data => this.setChannelInfo(data))));
  }

  analytics(token : string) : Observable<string[][]> {
    token = this.getToken(token);
    let params = [
      `${token}`,
      'dimensions=day',
      'endDate=2017-12-31',
      'ids=channel==MINE',
      'metrics=views',
      'sort=day',
      'startDate=2017-12-01',
      `key=${this.apiKey}`
    ].join("&")
    let queryUrl = `${this.analyticsUrl}${params}`;
    return this.http.get(queryUrl).pipe((map(data => this.setAnalytics(data))));
  }


}
