import { TestBed, inject } from '@angular/core/testing';

import { RestrictedService } from './restricted.service';

describe('RestrictedService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RestrictedService]
    });
  });

  it('should be created', inject([RestrictedService], (service: RestrictedService) => {
    expect(service).toBeTruthy();
  }));
});
