import { Component, OnInit } from '@angular/core';
import { RestrictedService } from './restricted.service';
import { Info } from './info.model';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  token : string;
  channelInfo : Info
  analytics : string[][];
  view : string;

  constructor(private restricted : RestrictedService) { }

  ngOnInit() {
    if (window.location.href.substring('http://localhost:4200/admin'.length).length > 0) {
      this.restricted.setAuth();
    }
    this.token = sessionStorage.getItem('auth');
  }

  auth() {
    this.restricted.auth();
  }

  getToken() : string {
    return this.token;
  }

  getView() : string {
    return this.view;
  }

  revokeAuth() {
    this.token = null;
    sessionStorage.removeItem('auth');
  }

  request(selezione : string) {
    if (selezione === 'channelInfo') {
      this.restricted.channel(this.token).subscribe(info => {
        this.channelInfo = info;
        this.view = 'channelInfo';
      });
    } else {
      this.restricted.analytics(this.token).subscribe(info => {
        this.analytics = info;
        this.view = 'report/12/2017';
      })
    }
  }

}
