import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { Routes , RouterModule, Router } from '@angular/router'
import { AuthGuard } from './auth.guard';
import { AuthService } from '../login/auth.service';
import { RestrictedService } from './restricted.service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

const routes : Routes = [
  {
    path : '',
    component : AdminComponent,
    pathMatch : 'full',
    canActivate : [AuthGuard]
  }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AdminComponent],
  providers : [AuthGuard,AuthService,RestrictedService]
})
export class AdminModule { }
