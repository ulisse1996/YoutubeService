import { Component, OnInit, OnChanges, DoCheck } from '@angular/core';
import { AuthService } from './auth.service';
import { Creds } from './login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{

  message : string;
  creds : Creds = new Creds('','');

  constructor(public authService : AuthService) {
    this.message = '';
   }

  ngOnInit() {
  }

  login() : boolean {
    this.message = '';
    if (!this.authService.login(this.creds.username,this.creds.password)) {
      this.message = "Username e/o password errati !";
      this.creds = new Creds('','');
      setTimeout(function() {
        this.message = "";
      }.bind(this),2500);
    } else {
      this.creds = new Creds('','');
    }
    return false;
  }

  logout() : boolean {
    this.authService.logout();
    return false;
  }

  isValid() : boolean {
    if (this.creds.username.length > 0 && this.creds.password.length > 4) {
      return true;
    }
    return false;
  }

}
