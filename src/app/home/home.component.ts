import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    navigator.geolocation.getCurrentPosition(function(location) {
      let tempLang = location.coords.latitude + ',' + location.coords.longitude;
      localStorage.setItem("pos",tempLang);
    });
  }

}
